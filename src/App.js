import React, { Component } from "react";
import { BrowserRouter as Router, Route } from "react-router-dom";
import "./App.css";
import Menu from "./components/Menu/Menu";

import CreateRequest from "./components/CreateRequest";
import ShowRequestList from "./components/ShowRequestList";
import ShowRequestDetails from "./components/ShowRequestDetails";
import UpdateRequestInfo from "./components/UpdateRequestInfo";
import HomePage from "./components/HomePage";
import AboutUs from "./components/AboutUs";
import SendSms from "./components/SendSms/SendSms";

class App extends Component {
  render() {
    return (
      <Router>
        <Menu />
        <div>
          <Route exact path="/" component={HomePage} />
          <Route exact path="/about-us" component={AboutUs} />
          <Route path="/available-requests" component={ShowRequestList} />
          <Route path="/create-request" component={CreateRequest} />
          <Route path="/edit-request/:id" component={UpdateRequestInfo} />
          <Route path="/show-request/:id" component={ShowRequestDetails} />
          <Route path="/send-sms/" component={SendSms} />
        </div>
      </Router>
    );
  }
}

export default App;
