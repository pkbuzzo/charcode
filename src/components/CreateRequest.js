import React, { Component } from "react";
import { Link } from "react-router-dom";
import "../App.css";
import axios from "axios";
import ReactFilestack from "filestack-react";

const image = [];

const basicOptions = {
  accept: "image/*",
  fromSources: ["local_file_system"],
  maxSize: 1024 * 1024,
  maxFiles: 1
};

class CreateRequest extends Component {
  constructor() {
    super();
    this.state = {
      name: "",
      city: "",
      state: "",
      description: "",
      phone_number: "",
      email: "",
      search_of: "",
      facebook_url: "",
      twitter_url: "",
      instagram_url: "",
      date_created: "",
      request_img: ""
    };
  }

  onChange = e => {
    this.setState({ [e.target.name]: e.target.value });
  };

  onSuccess = result => {
    image[0] = result.filesUploaded[0].url;
  };

  onError = error => {
    console.error("error", error);
  };

  onSubmit = e => {
    e.preventDefault();

    const data = {
      name: this.state.name,
      city: this.state.city,
      state: this.state.state,
      phone_number: this.state.phone_number,
      email: this.state.email,
      description: this.state.description,
      search_of: this.state.search_of,
      facebook_url: this.state.facebook_url,
      twitter_url: this.state.twitter_url,
      instagram_url: this.state.instagram_url,
      date_created: this.state.date_created,
      request_img: image[0]
    };

    axios
      .post("http://localhost:8082/api/jobs", data)
      .then(res => {
        this.setState({
          name: "",
          city: "",
          state: "",
          phone_number: "",
          email: "",
          description: "",
          search_of: "",
          facebook_url: "",
          twitter_url: "",
          instagram_url: "",
          date_created: "",
          request_img: ""
        });
        this.props.history.push("/available-requests");
      })
      .catch(err => {
        console.log("Error in CreateRequest!");
      });
    console.log(image[0]);
  };

  render() {
    const login = this.state.login;
    return (
      <div className="CreateRequest">
        <div className="container">
          <div className="row">
            <div className="col-md-8 m-auto">
              <br />
              <Link to="/" className="btn btn-outline-warning float-left">
                Show Request List
              </Link>
            </div>
            <div className="col-md-8 m-auto">
              <h1 className="display-4 text-center">Add Request</h1>
              <p className="lead text-center">Create new Request</p>

              <form noValidate onSubmit={this.onSubmit}>
                <div className="form-group">
                  <input
                    type="text"
                    placeholder="Name of the Organization"
                    name="name"
                    className="form-control"
                    value={this.state.name}
                    onChange={this.onChange}
                  />
                </div>
                <br />

                <div className="form-group">
                  <input
                    type="text"
                    placeholder="City of Organization"
                    name="city"
                    className="form-control"
                    value={this.state.city}
                    onChange={this.onChange}
                  />
                </div>

                <div className="form-group">
                  <input
                    type="text"
                    placeholder="State of Organization"
                    name="state"
                    className="form-control"
                    value={this.state.state}
                    onChange={this.onChange}
                  />
                </div>

                <div className="form-group">
                  <input
                    type="text"
                    placeholder="Phone Number"
                    name="phone_number"
                    className="form-control"
                    value={this.state.phone_number}
                    onChange={this.onChange}
                  />
                </div>

                <div className="form-group">
                  <input
                    type="text"
                    placeholder="Email"
                    name="email"
                    className="form-control"
                    value={this.state.email}
                    onChange={this.onChange}
                  />
                </div>

                <div className="form-group">
                  <input
                    type="text"
                    placeholder="Description of Organization's Mission"
                    name="description"
                    className="form-control"
                    value={this.state.description}
                    onChange={this.onChange}
                  />
                </div>

                <div className="form-group">
                  <input
                    type="text"
                    placeholder="What is the Organization in Search Of?"
                    name="search_of"
                    className="form-control"
                    value={this.state.search_of}
                    onChange={this.onChange}
                  />
                </div>
                <div className="form-group">
                  <input
                    type="text"
                    placeholder="Facebook Page"
                    name="facebook_url"
                    className="form-control"
                    value={this.state.facebook_url}
                    onChange={this.onChange}
                  />
                </div>
                <div className="form-group">
                  <input
                    type="text"
                    placeholder="Twitter Page"
                    name="twitter_url"
                    className="form-control"
                    value={this.state.twitter_url}
                    onChange={this.onChange}
                  />
                </div>
                <div className="form-group">
                  <input
                    type="text"
                    placeholder="Instagram Page"
                    name="instagram_url"
                    className="form-control"
                    value={this.state.instagram_url}
                    onChange={this.onChange}
                  />
                </div>
                <div className="form-group">
                  <input
                    type="date"
                    placeholder="Date of Submission"
                    name="date_created"
                    className="form-control"
                    value={this.state.date_created}
                    onChange={this.onChange}
                  />
                </div>
                <ReactFilestack
                  id="img_upload"
                  apikey="AlVpam4pwRQe6RtLCspFbz"
                  buttonText="Upload Photo"
                  buttonClass="ui medium button gray"
                  options={basicOptions}
                  height="250px"
                  width="250px"
                  onSuccess={this.onSuccess}
                  onError={this.onError}
                />

                <input
                  type="submit"
                  className="btn btn-outline-warning btn-block mt-4"
                />
              </form>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default CreateRequest;
