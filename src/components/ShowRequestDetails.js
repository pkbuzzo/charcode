import React, { Component } from "react";
import ReactDOM from "react-dom";
import { Link } from "react-router-dom";
import "./ShowRequestDetails.css";
import axios from "axios";
// import PropTypes from "prop-types";

// export const env = {
//   REACT_APP_EMAILJS_RECEIVER: process.env.REACT_APP_EMAILJS_RECEIVER,
//   REACT_APP_EMAILJS_TEMPLATEID: process.env.REACT_APP_EMAILJS_TEMPLATEID,
//   REACT_APP_EMAILJS_USERID: process.env.REACT_APP_EMAILJS_USERID
// };

// let email = [];

// class FeedbackForm extends Component {
//   state = {
//     feedback: "",
//     formSubmitted: false
//   };

//   handleCancel = this.handleCancel.bind(this);
//   handleChange = this.handleChange.bind(this);
//   handleSubmit = this.handleSubmit.bind(this);

//   static sender = "sender@example.com";

//   handleCancel() {
//     this.setState({
//       feedback: "",
//       senderEmail: ""
//     });
//   }

//   handleChange(event) {
//     this.setState({
//       ...this.state,
//       feedback: event.target.value
//     });
//   }

//   handleChangeEmail(event) {
//     email[0] = event.target.value;
//   }

//   handleSubmit(event) {
//     event.preventDefault();

//     const {
//       REACT_APP_EMAILJS_RECEIVER: receiverEmail,
//       REACT_APP_EMAILJS_TEMPLATEID: template,
//       REACT_APP_EMAILJS_USERID: user
//     } = env;

//     this.sendFeedback(
//       template,
//       this.sender,
//       receiverEmail,
//       this.state.feedback,
//       user
//     );

//     this.setState({
//       formSubmitted: true
//     });
//   }

//   // Note: this is using default_service, which will map to whatever
//   // default email provider you've set in your EmailJS account.
//   sendFeedback(templateId, senderEmail, receiverEmail, feedback, user) {
//     window.emailjs
//       .send(
//         "default_service",
//         templateId,
//         {
//           senderEmail,
//           receiverEmail,
//           feedback
//         },
//         user
//       )
//       .then(res => {
//         this.setState({
//           formEmailSent: true
//         });
//       })
//       // Handle errors here however you like
//       .catch(err => console.error("Failed to send feedback. Error: ", err));
//   }

//   render() {
//     return (
//       <form className="feedback-form" onSubmit={this.handleSubmit}>
//         <h1>Your Feedback</h1>
//         <textarea
//           className="text-input"
//           id="feedback-entry"
//           name="feedback-entry"
//           onChange={this.handleChange}
//           placeholder="Enter your feedback here"
//           required
//           value={this.state.feedback}
//         />
//         <textarea
//           id="email"
//           className="text-input"
//           rows={1}
//           onChange={this.handleChangeEmail}
//           placeholder="Enter your email here"
//           required
//           value={this.state.senderEmail}
//         />
//         <div className="btn-group">
//           <button className="btn btn--cancel" onClick={this.handleCancel}>
//             Cancel
//           </button>
//           <input type="submit" value="Submit" className="btn btn--submit" />
//         </div>
//       </form>
//     );
//   }
// }

const id = [];

class showRequestDetails extends Component {
  constructor(props) {
    super(props);
    this.state = {
      request: {},
      recipient: "",
      textmessage: "",
      contactNumber: "",
      contactEmail: ""
    };
  }

  // textInfo = {
  //   text: {
  //     recipient: "",
  //     textmessage: ""
  //   }
  // };

  componentDidMount() {
    // console.log("Print id: " + this.props.match.params.id);
    axios
      .get("http://localhost:8082/api/jobs/" + this.props.match.params.id)
      .then(res => {
        // console.log("Print-showrequestDetails-API-response: " + res.data);
        this.setState({
          ...this.state,
          request: res.data
        });
        id.push(this.props.match.params.id);
        console.log(res.data);
      })
      .catch(err => {
        console.log("Error from ShowRequestDetails");
      });
  }

  onDeleteClick(id) {
    axios
      .delete("http://localhost:8082/api/jobs/" + id)
      .then(res => {
        this.props.history.push("/");
      })
      .catch(err => {
        console.log("Error form ShowRequestDetails_deleteClick");
      });
  }

  sendText = () => {
    const div = document.getElementById("popup1");
    const text = this.state;
    const message = `${text.textmessage}. My contact number is ${text.contactNumber} and my email is ${text.contactEmail}. I look forward to hearing from you!`;
    //pass text message GET variables via query string
    fetch(
      `http://localhost:3000/send-text?recipient=${text.request.phone_number}&textmessage=${message}`
    )
      .then(div.classList.toggle("overlay:target"))
      .catch(err => console.error(err));
  };

  render() {
    const request = this.state.request;

    const text = this.state;
    const spacer = {
      margin: 8
    };
    const textArea = {
      borderRadius: 4
    };

    let PopUp = (
      <div>
        <div class="box">
          <a class="button" href="#popup1">
            Contact This Organization
          </a>
        </div>

        <div id="popup1" class="overlay">
          <div class="popup">
            <h2 id="spechead">Contact</h2>
            <a class="close" href="#">
              &times;
            </a>
            <div class="content">
              <div className="App">
                <div style={{ marginTop: 10 }}>
                  <h2> Send Text Message </h2>
                  {/* <label> Your Phone Number </label>
                  <br />
                  <input
                    value={text.recipient}
                    onChange={e =>
                      this.setState({
                        ...this.state,
                        recipient: e.target.value
                      })
                    }
                  /> */}
                  <div style={spacer} />
                  <label id="labelTwo"> Message: </label>
                  <br />
                  <textarea
                    rows={3}
                    value={text.textmessage}
                    style={textArea}
                    onChange={e =>
                      this.setState({
                        ...this.state,
                        textmessage: e.target.value
                      })
                    }
                  />
                  <label id="labelOne">
                    Enter A Valid Email For The Organization To Contact you At:
                  </label>
                  <textarea
                    rows={1}
                    value={text.contactEmail}
                    style={textArea}
                    onChange={e =>
                      this.setState({
                        ...this.state,
                        contactEmail: e.target.value
                      })
                    }
                  />
                  <div style={spacer} />
                  <button onClick={this.sendText} href="#">
                    Send Text
                  </button>
                  {/* <a onClick={this.sendText} class="close" href="#">
                    Send Text
                  </a> */}
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    );

    let RequestItem = (
      <div>
        <table className="table table-hover table-dark">
          {/* <thead>
          <tr>
            <th scope="col">#</th>
            <th scope="col">First</th>
            <th scope="col">Last</th>
            <th scope="col">Handle</th>
          </tr>
        </thead> */}
          <tbody>
            <tr>
              <th scope="row">1</th>
              <td>Name of Organization</td>
              <td>{request.name}</td>
            </tr>
            <tr>
              <th scope="row">2</th>
              <td>City of Organization</td>
              <td>{request.city}</td>
            </tr>
            <tr>
              <th scope="row">3</th>
              <td>State of Organization</td>
              <td>{request.state}</td>
            </tr>
            <tr>
              <th scope="row">4</th>
              <td>Organization's Mission</td>
              <td>{request.description}</td>
            </tr>
            <tr>
              <th scope="row">5</th>
              <td>What the Organization is Searching For</td>
              <td>{request.search_of}</td>
            </tr>
            <tr>
              <th scope="row">6</th>
              <td>Organization's Facebook Page</td>
              <td>{request.facebook_url || "Not Available"}</td>
            </tr>
            <tr>
              <th scope="row">7</th>
              <td>Organization's Twitter Page</td>
              <td>{request.twitter_url || "Not Available"}</td>
            </tr>
            <tr>
              <th scope="row">8</th>
              <td>Organization's Instagram Page</td>
              <td>{request.instagram_url || "Not Available"}</td>
            </tr>
            <tr>
              <th scope="row">9</th>
              <td>Date Request was Published</td>
              <td>{request.date_created}</td>
            </tr>
          </tbody>
        </table>
      </div>
    );

    return (
      <div className="ShowRequestDetails">
        <div className="container">
          <div className="row">
            <div className="col-md-10 m-auto">
              <br /> <br />
              <Link to="/" className="btn btn-outline-warning float-left">
                Show Request List
              </Link>
            </div>
            <br />
            <div className="col-md-8 m-auto">
              <h1 className="display-4 text-center">Request's Record</h1>
              <p className="lead text-center">View Request's Info</p>
              <hr /> <br />
            </div>
          </div>
          <div>{RequestItem}</div>

          <div className="row">
            <div className="col-md-6">
              <button
                type="button"
                className="btn btn-outline-danger btn-lg btn-block"
                onClick={this.onDeleteClick.bind(this, request._id)}
              >
                Delete Request
              </button>
              <br />
            </div>

            <div className="col-md-6">
              <Link
                to={`/edit-request/${request._id}`}
                className="btn btn-outline-info btn-lg btn-block"
              >
                Edit Request
              </Link>
              {PopUp}

              <br />
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default showRequestDetails;
