import React, { Component } from "react";
import "../App.css";
import axios from "axios";
import { Link } from "react-router-dom";
import RequestCard from "./RequestCard";

class ShowRequestList extends Component {
  constructor(props) {
    super(props);
    this.state = {
      requests: []
    };
  }

  componentDidMount() {
    axios
      .get("http://localhost:8082/api/jobs")
      .then(res => {
        this.setState({
          requests: res.data
        });
      })
      .catch(err => {
        console.log("Error from ShowRequestList");
      });
  }

  render() {
    const requests = this.state.requests;
    console.log("PrintRequest: " + requests);
    let requestList;

    if (!requests) {
      requestList = "there is no request record!";
    } else {
      requestList = requests
        .reverse()
        .map((request, k) => <RequestCard request={request} key={k} />);
    }

    return (
      <div className="ShowRequestList">
        <div className="container">
          <div className="row">
            <div className="col-md-12">
              <br />
              <h2 className="display-4 text-center">Requests List</h2>
            </div>

            <div className="col-md-11">
              <Link
                to="/create-request"
                className="btn btn-outline-warning float-right"
              >
                + Add New Request
              </Link>
              <br />
              <br />
              <hr />
            </div>
          </div>

          <div className="list">{requestList}</div>
        </div>
      </div>
    );
  }
}

export default ShowRequestList;
