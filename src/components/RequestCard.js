import React from "react";
import { Link } from "react-router-dom";
import "../App.css";

const RequestCard = props => {
  const request = props.request;

  return (
    <div className="card-container">
      <img
        className="request-img"
        src={
          request.request_img ||
          "https://freerangestock.com/sample/81221/profile-image.jpg"
        }
        alt=""
      />
      <div className="desc">
        <h2>
          <Link to={`/show-request/${request._id}`}>{request.name}</Link>
        </h2>
        <h3>
          {request.city},{request.state}
        </h3>
        <p>{request.description}</p>
      </div>
    </div>
  );
};

export default RequestCard;
