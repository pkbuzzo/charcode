import React from "react";
import { Link } from "react-router-dom";
import "./Menu.css";

class Menu extends React.Component {
  handleLogin = event => {
    event.preventDefault();
    this.props.login();
  };

  render() {
    return (
      <div id="menu">
        <h1 id="h1" class="text-shadow underline underline--double">
          CharCode
        </h1>
        <Link className="menuLinks" activeClassName="selected" to="/">
          Home
        </Link>
        <Link className="menuLinks" activeClassName="selected" to="/about-us">
          About Us
        </Link>
        <Link
          className="menuLinks"
          activeClassName="selected"
          to="/available-requests"
        >
          Available Requests
        </Link>
        <form>
          <input type="text" size="10" placeholder="User Name"></input>
          <input type="password" size="10" placeholder="Password"></input>
          <button type="submit">
            <Link to="/">Log In</Link>
          </button>
        </form>
      </div>
    );
  }
}

const mapStateToProps = state => {
  return {
    username: state.auth.login.result && state.auth.login.result.username
  };
};

export default Menu;
