import React, { Component } from "react";
import "./HomePage.css";
import axios from "axios";
import { Link } from "react-router-dom";
import RequestCard from "./RequestCard";

class HomePage extends Component {
  render() {
    return (
      <div>
        <header class="showcase">
          <div class="content">
            <img
              src="https://i.ya-webdesign.com/images/3d-letter-c-png-1.png"
              class="logo"
              alt="Traversy Media"
            />
            <div class="title">Welcome To CharCode</div>
            <div class="text">
              Connecting Talented Programmers and Non-Profits To Bring About
              Great Websites for Greater Causes
            </div>
          </div>
        </header>

        <section class="services">
          <div class="container grid-3 center">
            <div>
              <i class="fab fas fa-desktop fa-3x"></i>
              <h3>Enhance Your Portfolio</h3>
              <p>
                Developers Are Given The Opportunity To Apply Their Skills And
                Create Amazing Web Applications To Build Both Their Resumes And
                Help Their Community
              </p>
            </div>
            <div>
              <i class="fas fa-gift fa-3x"></i>
              <h3>Websites For Charity</h3>
              <p>
                Organizations That Go Without A Website In Now-A-Days Are
                Missing Out On an Incredible Amount Of Exposure. Our Developers
                Solve That Once And For All
              </p>
            </div>
            <div>
              <i class="fas fa-globe fa-3x"></i>
              <h3>Create Community</h3>
              <p>
                Our Community Allows For Real-World Connections To Be Made— A
                Truly Unique Platform Where Such Connections To Flourish Into
                Professional Relationships
              </p>
            </div>
          </div>
        </section>

        <section class="about bg-light">
          <div class="container">
            <div class="grid-2">
              <div class="center">
                <i class="fas fa-laptop-code fa-10x"></i>
              </div>
              <div>
                <h3>Creating Win-Win Scenarios For All Parties Involved</h3>
                <br></br>
                <p>
                  CharCode Connects Non-Profits In Search Of A Web Application,
                  With Developers Looking To Prove Their Skills To Their Next
                  Potential Employer. This Creates A Beneficial Opportunity For
                  Both The Organizations Receiving Their New Applications, As
                  Well As The Developer Who Has Created A Resume Builder That
                  Will Seperate Them From The Rest Of The Pack.
                </p>
              </div>
            </div>
          </div>
        </section>
      </div>
    );
  }
}

export default HomePage;
