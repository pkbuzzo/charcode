import React, { Component } from "react";
import { Link } from "react-router-dom";
import axios from "axios";
import "../App.css";

class UpdateRequestInfo extends Component {
  constructor(props) {
    super(props);
    this.state = {
      name: "",
      city: "",
      state: "",
      description: "",
      search_of: "",
      facebook_url: "",
      twitter_url: "",
      instagram_url: "",
      date_created: ""
    };
  }

  componentDidMount() {
    // console.log("Print id: " + this.props.match.params.id);
    axios
      .get("http://localhost:8082/api/jobs/" + this.props.match.params.id)
      .then(res => {
        // this.setState({...this.state, request: res.data})
        this.setState({
          name: res.data.name,
          city: res.data.city,
          state: res.data.state,
          description: res.data.description,
          search_of: res.data.search_of,
          facebook_url: res.data.facebook_url,
          twitter_url: res.data.twitter_url,
          instagram_url: res.data.instagram_url,
          date_created: res.data.date_created
        });
      })
      .catch(err => {
        console.log("Error from UpdateRequestInfo");
      });
  }

  onChange = e => {
    this.setState({ [e.target.name]: e.target.value });
  };

  onSubmit = e => {
    e.preventDefault();

    const data = {
      name: this.state.name,
      city: this.state.city,
      state: this.state.state,
      description: this.state.description,
      search_of: this.state.search_of,
      facebook_url: this.state.facebook_url,
      twitter_url: this.state.twitter_url,
      instagram_url: this.state.instagram_url,
      date_created: this.state.date_created
    };

    axios
      .put("http://localhost:8082/api/jobs/" + this.props.match.params.id, data)
      .then(res => {
        this.props.history.push("/show-request/" + this.props.match.params.id);
      })
      .catch(err => {
        console.log("Error in UpdateRequestInfo!");
      });
  };

  render() {
    return (
      <div className="UpdateRequestInfo">
        <div className="container">
          <div className="row">
            <div className="col-md-8 m-auto">
              <br />
              <Link to="/" className="btn btn-outline-warning float-left">
                Show Request List
              </Link>
            </div>
            <div className="col-md-8 m-auto">
              <h1 className="display-4 text-center">Edit Request</h1>
              <p className="lead text-center">Update Request's Info</p>
            </div>
          </div>

          <div className="col-md-8 m-auto">
            <form noValidate onSubmit={this.onSubmit}>
              <div className="form-group">
                <label htmlFor="name">Name of Organization</label>
                <input
                  type="text"
                  placeholder="Name of Organization"
                  name="name"
                  className="form-control"
                  value={this.state.name}
                  onChange={this.onChange}
                />
              </div>
              <br />

              <div className="form-group">
                <label htmlFor="city">City of Organization</label>
                <input
                  type="text"
                  placeholder="City of Organization"
                  name="city"
                  className="form-control"
                  value={this.state.city}
                  onChange={this.onChange}
                />
              </div>

              <div className="form-group">
                <label htmlFor="state">State of Organiation</label>
                <input
                  type="text"
                  placeholder="State of Organization"
                  name="state"
                  className="form-control"
                  value={this.state.state}
                  onChange={this.onChange}
                />
              </div>

              <div className="form-group">
                <label htmlFor="description">
                  Description of Organization's Mission
                </label>
                <input
                  type="text"
                  placeholder="Description of Organization's Mission"
                  name="description"
                  className="form-control"
                  value={this.state.description}
                  onChange={this.onChange}
                />
              </div>

              <div className="form-group">
                <label htmlFor="search_of">
                  What the Organization is in Search Of
                </label>
                <input
                  type="text"
                  placeholder="What the Organization is in Search Of"
                  name="search_of"
                  className="form-control"
                  value={this.state.search_of}
                  onChange={this.onChange}
                />
              </div>
              <div className="form-group">
                <label htmlFor="facebook_url">
                  Organization's Facebook Page
                </label>
                <input
                  type="text"
                  placeholder="Organization's Facebook Page"
                  name="facebook_url"
                  className="form-control"
                  value={this.state.facebook_url}
                  onChange={this.onChange}
                />
              </div>
              <div className="form-group">
                <label htmlFor="twitter_url">Organization's Twitter Page</label>
                <input
                  type="text"
                  placeholder="Organization's Twitter Page"
                  name="twitter_url"
                  className="form-control"
                  value={this.state.twitter_url}
                  onChange={this.onChange}
                />
              </div>
              <div className="form-group">
                <label htmlFor="instagram_url">
                  Organization's Instagram Page
                </label>
                <input
                  type="text"
                  placeholder="Organization's Instagram Page"
                  name="instagram_url"
                  className="form-control"
                  value={this.state.instagram_url}
                  onChange={this.onChange}
                />
              </div>
              <div className="form-group">
                <label htmlFor="date_created">Date of Revision</label>
                <input
                  type="date"
                  placeholder="Date of Revision"
                  name="date_created"
                  className="form-control"
                  value={this.state.date_created}
                  onChange={this.onChange}
                />
              </div>

              <button
                type="submit"
                className="btn btn-outline-info btn-lg btn-block"
              >
                Update Request
              </button>
            </form>
          </div>
        </div>
      </div>
    );
  }
}

export default UpdateRequestInfo;
